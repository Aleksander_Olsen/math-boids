
#include <SDL.h>
#include <SDL_image.h>

#include <stdio.h>
#include <ctime>

#include "Globals.h"
#include "Flock.h"
#include "SDLClass.h"

//The window we'll be rendering to
SDL_Window* gWindow = nullptr;

//The SDL renderer.
SDL_Renderer* gRenderer = nullptr;

//Triangle texture.
SDL_Texture* boidTexture = nullptr;

//An SDL class for handeling render and texture loading.
std::unique_ptr<SDLClass> SDL_Class;

//Clock used to calculate delta time.
Uint32 ticks;
Uint32 lastTicks;
float dt;

std::unique_ptr<Flock> flock;

//The application will quit if this is true.
bool gQuit = false;

//Starts up SDL, creates window, and initializes OpenGL
bool init();

//Per frame update
void update(float _dt);

//Renders quad to the screen
void render();

//Shut down procedure
void close();

//Initialize textures.
void initTexture();



bool init() {
	//Initialization flag
	bool success = true;

	SDL_Class = std::make_unique<SDLClass>();

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	}
	else {
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
			printf("Warning: Linear texture filtering not enabled!");
		}

		//Create window
		gWindow = SDL_CreateWindow("Assignment 1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH + 1, SCREEN_HEIGHT + 1, SDL_WINDOW_SHOWN);
		if (gWindow == NULL) {
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else {

			//Create renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
			if (gRenderer == NULL) {
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else {
				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags)) {
					printf("SDL_image could not initialize! SDL_mage Error: %s\n", IMG_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

void handleKeys(unsigned char key, int x, int y) {
	//Toggle cube
	if (key == 'q') {
		gQuit = true;
	} else if (key == 'r') {
		
	}
}

void events(SDL_Event& eventHandler) {
	//Handle events on queue
	while (SDL_PollEvent(&eventHandler) != 0) {
		//User requests quit
		if (eventHandler.type == SDL_QUIT) {
			gQuit = true;
			//Handle keypress with current mouse position
		}
		else if (eventHandler.type == SDL_TEXTINPUT) {
			int x = 0, y = 0;
			SDL_GetMouseState(&x, &y);
			handleKeys(eventHandler.text.text[0], x, y);
		}
	}
}

void update(float _dt) {
	flock->update(_dt);
}

void render() {
	//Clear screen
	SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0x00);
	SDL_RenderClear(gRenderer);

	//Render boid
	flock->render(SDL_Class, boidTexture, gRenderer);

	//Update screen
	SDL_RenderPresent(gRenderer);
}

void close() {

	//Destroy window	
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//t SDL subsystems
	SDL_Quit();
}

void initTexture() {
	SDL_Class->FreeTexture(boidTexture);
	boidTexture = SDL_Class->LoadTexture(BOIDPNG, gRenderer);
}



int main(int argc, char* args[]) {

	srand(time(0));

	//Start up SDL and create window
	if (!init()) {
		printf("Failed to initialize!\n");
	}
	else {
		//Main loop flag
		gQuit = false;

		initTexture();

		//Get the current clock time
		ticks = SDL_GetTicks();
		lastTicks = ticks;

		flock = std::make_unique<Flock>();

		//Event handler
		SDL_Event eventHandler;

		//Enable text input
		SDL_StartTextInput();

		//While application is running
		while (!gQuit) {
			//Handle events
			events(eventHandler);


			//Get ticks and calculate delta time
			ticks = SDL_GetTicks();
			dt = (ticks - lastTicks) / 1000.0;
			lastTicks = ticks;

			//Handle updates
			update(dt);

			//Render quad
			render();
		}

		//Disable text input
		SDL_StopTextInput();
	}

	//Free resources and close SDL
	close();

	return 0;
}