#pragma once

#include <SDL.h>
#include <SDL_image.h>

#include "Globals.h"
#include "Boid.h"
#include "SDLClass.h"

class Flock {
private:
	std::vector<std::unique_ptr<Boid>> boids;
public:
	Flock();
	~Flock();

	glm::vec2 cohesion(std::unique_ptr<Boid> &curBoid);
	glm::vec2 align(std::unique_ptr<Boid> &curBoid);
	glm::vec2 separate(std::unique_ptr<Boid> &curBoid);

	void update(float dt);
	void render(std::unique_ptr<SDLClass> &SDL_Class, SDL_Texture* tex, SDL_Renderer* gRenderer);
};

