#pragma once

#include <gl\glew.h>
#include <gl\glu.h>


#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <vector>
#include <iostream>
#include <memory>
#include <cmath>


//Screen dimension constants
const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

const int NUM_BOIDS = 40;
const float MAX_SPEED = 150.0;

const std::string BOIDPNG = "Boid2.png";

const SDL_Rect boidRect = { 0, 0, 16, 8 };
