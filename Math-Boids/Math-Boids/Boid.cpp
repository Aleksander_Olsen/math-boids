#include "Boid.h"



Boid::Boid() {
	pos.x = (rand() % (SCREEN_WIDTH - 16) + 8);
	pos.y = (rand() % (SCREEN_HEIGHT - 16) + 8);
	ang = rand() % 360;
	vel.x = glm::cos(ang * (180.0 / M_PI));
	vel.y = glm::sin(ang * (180.0 / M_PI));
	acc = { 1.00008, 1.00008 };
}


Boid::~Boid()
{
}

void Boid::wrapAround() {
	if (pos.x < 0) {
		pos.x = SCREEN_WIDTH;
	}
	if (pos.x - 16 > SCREEN_WIDTH) {
		pos.x = 0;
	}
	if (pos.y < 0) {
		pos.y = SCREEN_HEIGHT;
	}
	if (pos.y - 8 > SCREEN_HEIGHT) {
		pos.y = 0;
	}
}

void Boid::update(float dt) {
	
	vel += newVel;
	//Slowley accelerate
	vel = glm::normalize(vel) * MAX_SPEED;
	//Keep velocity from going abow max speed.
	vel = glm::clamp(vel, -MAX_SPEED, MAX_SPEED);
	//Change position.
	pos += vel * dt;

	wrapAround();
	setRotation();

}

void Boid::render(std::unique_ptr<SDLClass> &SDL_Class, SDL_Texture* tex, SDL_Renderer* gRenderer) {
	SDL_Class->Render(pos.x - 16, pos.y - 8, tex, boidRect, gRenderer, ang);
}

void Boid::setRotation() {
	ang = atan2(vel.y, vel.x) * 180.0 / M_PI;
}

void Boid::setUpdatedVel(glm::vec2 _vel) {
	newVel = _vel;
}

glm::vec2 Boid::getPos() {
	return pos;
}

glm::vec2 Boid::getVel() {
	return vel;
}
