#include "Flock.h"



Flock::Flock() {
	for (int i = 0; i < NUM_BOIDS; i++) {
		boids.push_back(std::make_unique<Boid>());
	}
}


Flock::~Flock()
{
}

glm::vec2 Flock::cohesion(std::unique_ptr<Boid> &curBoid) {
	glm::vec2 coh = { 0, 0 };
	float dist = 100.0;
	float weight = 3.0;
	int count = 0;
	
	for (auto &x : boids) {
		if (x != curBoid) {
			float d = glm::distance(x->getPos(), curBoid->getPos());
			if ((d > 0.0) && (d < dist)) {
				coh += x->getPos();
				++count;
			}
		}
	}

	if (count > 0) {
		coh /= (count);
		coh = glm::normalize(coh - curBoid->getPos());
		return coh * weight;
	} else {
		return coh;
	}

	
}

glm::vec2 Flock::align(std::unique_ptr<Boid> &curBoid) {
	glm::vec2 align = { 0, 0 };
	float dist = 50.0;
	float weight = 2.0;
	int count = 0;
	
	for (auto &x : boids) {
		if (x != curBoid) {
			float d = glm::distance(curBoid->getPos(), x->getPos());
			if (d < 0.0) {
				d *= -1;
			}
			if ((d > 0.0) && (d < dist)) {
				align += x->getVel();
				++count;
			}
		}
	}

	if (count > 0) {
		align /= static_cast<float>(count);
		align = glm::normalize(align - curBoid->getVel());
		return align * weight;
	} else {
		return align;
	}

	
}

glm::vec2 Flock::separate(std::unique_ptr<Boid>& curBoid) {
	glm::vec2 sep = { 0, 0 };
	float dist = 25.0;
	float weight = 6.0;
	int count = 0;

	for (auto &x : boids) {
		if (x != curBoid) {
			float d = glm::distance(curBoid->getPos(), x->getPos());
			if (d > 0 && d < dist) {
				glm::vec2 diff = curBoid->getPos() - x->getPos();
				diff = glm::normalize(diff) / d;
				sep += diff;
				++count;
			}
		}
	}

	if (count > 0) {
		sep /= static_cast<float>(count);
	}

	if (glm::length(sep) > 0.0) {
		sep = glm::normalize(sep);
	}
	

	return sep * weight;
}

void Flock::update(float dt) {
	glm::vec2 v1 = { 0, 0 };
	glm::vec2 v2 = { 0, 0 };
	glm::vec2 v3 = { 0, 0 };
	for (auto &x : boids) {
		v1 = cohesion(x);
		v2 = align(x);
		v3 = separate(x);
		x->setUpdatedVel((v1 + v2 + v3));
	}

	for (auto &x : boids) {
		x->update(dt);
	}
}

void Flock::render(std::unique_ptr<SDLClass>& SDL_Class, SDL_Texture * tex, SDL_Renderer * gRenderer) {
	for (auto &x : boids) {
		x->render(SDL_Class, tex, gRenderer);
	}
}
