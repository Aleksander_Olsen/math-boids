#pragma once

#include <SDL.h>
#include <SDL_image.h>

#include "Globals.h"
#include "SDLClass.h"

class Boid {
private:
	glm::vec2 pos;
	glm::vec2 vel;
	glm::vec2 acc;
	glm::vec2 newVel;
	float ang;
public:
	Boid();
	~Boid();

	void wrapAround();

	void update(float dt);
	void render(std::unique_ptr<SDLClass> &SDL_Class, SDL_Texture* tex, SDL_Renderer* gRenderer);

	void setRotation();
	void setUpdatedVel(glm::vec2 _vel);
	glm::vec2 getPos();
	glm::vec2 getVel();
};

